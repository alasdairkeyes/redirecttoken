<?php

namespace RedirectToken\Exception;

use Exception;

class InvalidSecretKeyException extends Exception
{
}
