<?php

include_once(__DIR__ . '/../vendor/autoload.php');
include_once(__DIR__ . '/secret.inc.php');

use RedirectToken\Generator;
use GuzzleHttp\Psr7\Uri;

$uriDestination = new Uri('https://www.google.co.uk/');

// Instantiate token generator and create token
$generator = new Generator($secretKey);
$token = $generator->generateToken($uriDestination);

$validRedirectUri = 'redirect.php?' . http_build_query([ 'uri' => (string)$uriDestination, 'token' => $token ], '', '&');
$invalidRedirectUri = 'redirect.php?' . http_build_query([ 'uri' => (string)$uriDestination, 'token' => 'invalidtoken' ], '', '&');

// Output
print '<a href="' . $validRedirectUri . '">Valid redirect</a>' . PHP_EOL;
print "<br/>";
print '<a href="' . $invalidRedirectUri . '">Invalid redirect</a>' . PHP_EOL;
