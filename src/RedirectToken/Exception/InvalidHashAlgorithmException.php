<?php

namespace RedirectToken\Exception;

use Exception;

class InvalidHashAlgorithmException extends Exception
{
}
